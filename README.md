# myadpl-landing-page

This repository keeps source code and assets of the myad.pl landing page.

#### Requirements
1. composer (https://getcomposer.org/download/)
2. php >= 7.1
3. git client
4. grpc client - https://cloud.google.com/php/grpc

#### Deployment
1. Checkout repository
2. Install dependencies by running ``composer install``

#### Development
1. Run `` php bin/console server:start`` to run local http server
2.
