require('../css/bootstrap-datetimepicker.min.css');
require('../css/app.css');

require('jquery');
require('jquery-ui');
require('./bootstrap-datetimepicker.min.js');

$(function () {
    $('.image_source').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        console.log(fileName);
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    });

    $(".form_datetime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        autoclose: true,
        minuteStep: 30,
        pickSeconds: true,
    });
});
