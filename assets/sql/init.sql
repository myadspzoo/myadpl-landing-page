INSERT INTO user(email, password, roles) VALUES
('marcin.boron@myad.pl', '$argon2i$v=19$m=1024,t=2,p=2$cnI5elRRV2k4b0dKS1MvSg$Trhjm3f2oLByILCh/mOzpSsL/VoCo9ggZ68MDauwW2c', '["ROLE_SUPER_ADMIN"]'),
('rafal.bociek@myad.pl', '$argon2i$v=19$m=1024,t=2,p=2$NnZLaUJPZmxTWjFjZThVbQ$ro0MgEW1tnfh+uTgkmy59t7G8aH67ilrYnGEWYettek', '["ROLE_ADMIN"]'),
('piotr.marzec@myad.pl', '$argon2i$v=19$m=1024,t=2,p=2$NnZLaUJPZmxTWjFjZThVbQ$ro0MgEW1tnfh+uTgkmy59t7G8aH67ilrYnGEWYettek', '["ROLE_ADMIN"]'),
('emil.paradowski@myad.pl', '$argon2i$v=19$m=1024,t=2,p=2$NnZLaUJPZmxTWjFjZThVbQ$ro0MgEW1tnfh+uTgkmy59t7G8aH67ilrYnGEWYettek', '["ROLE_ADMIN"]')
;