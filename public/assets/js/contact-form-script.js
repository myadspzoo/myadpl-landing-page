/*==============================================================*/
// Addax Contact Form  JS
/*==============================================================*/
(function ($) {
    "use strict"; // Start of use strict
    $(".contactForm").validator().on("submit", function (event) {
        var form = $(this); //wrap this in jQuery
        var formId = form.attr('id');
        if (event.isDefaultPrevented()) {
            // handle the invalid form...
            formError(formId);
            submitMSG(formId, false, "Did you fill in the form properly?");
        } else {
            // everything looks good!
            event.preventDefault();
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function (text) {
                    if (text == "success") {
                        form.trigger("reset");
                        submitMSG(formId, true, "Message Submitted!");
                    } else {
                        formError(formId);
                        submitMSG(formId, false, text);
                    }
                }
            });
        }
    });

    function formError(formId) {
        $("."+formId).removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $(this).removeClass();
        });
    }

    function submitMSG(formId, valid, msg) {
        if (valid) {
            var msgClasses = "h4 text-left tada animated text-success";
        } else {
            var msgClasses = "h4 text-left text-danger";
        }
        $("#" + formId + "MsgSubmit").removeClass().addClass(msgClasses).text(msg);
    }
}(jQuery)); // End of use strict