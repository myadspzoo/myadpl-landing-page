<?php

namespace App\Controller;

use App\Entity\Classified;
use App\Entity\Device;
use App\Entity\Image;
use App\Entity\Partner;
use App\Entity\Playlist;
use App\Entity\Video;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Storage\StorageClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function adminDashboard(EntityManagerInterface $entityManager)
    {
        $partners = [];
        $devices = [];
        $playlists = [];
        $classifieds = [];
        $images = [];
        $videos = [];

        $partners['local'] = $entityManager->getRepository(Partner::class)->findAll();
        $partners['sync'] = $entityManager->getRepository(Partner::class)->findAllSynchronized();

        $devices['local'] = $entityManager->getRepository(Device::class)->findAll();
        $devices['sync'] = $entityManager->getRepository(Device::class)->findAllSynchronized();

        $classifieds['local'] = $entityManager->getRepository(Classified::class)->findAll();
        $classifieds['sync'] = $entityManager->getRepository(Classified::class)->findAllSynchronized();

        $images['local'] = $entityManager->getRepository(Image::class)->findAll();
        $images['sync'] = $entityManager->getRepository(Image::class)->findAllSynchronized();

        $videos['local'] = $entityManager->getRepository(Video::class)->findAll();
        $videos['sync'] = $entityManager->getRepository(Video::class)->findAllSynchronized();

        try {
            $firestore = new FirestoreClient();

            $partners['firebase'] = $firestore->collection(Partner::COLLECTION)->documents();

            $devices['firebase'] = $firestore->collection(Device::COLLECTION)->documents();

            $classifieds['firebase'] = $firestore->collection(Classified::COLLECTION)->documents();

            $images['firebase'] = $firestore->collection(Image::COLLECTION)->documents();

            $videos['firebase'] = $firestore->collection(Video::COLLECTION)->documents();

            $playlists = $firestore->collection(Playlist::COLLECTION)->documents();

        } catch (GoogleException $e) {

        } catch (\Exception $e) {

        }

        return $this->render('admin/dasboard.html.twig', [
            'partners' => $partners,
            'devices' => $devices,
            'playlists' => $playlists,
            'classifieds' => $classifieds,
            'images' => $images,
            'videos' => $videos
        ]);
    }

    /**
     * @Route("/collection-remove/{name}", name="collection_remove", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param $name
     * @return Response
     */
    public function collectionRemove(EntityManagerInterface $entityManager, $name): Response
    {
        $class_name = "App\Entity\\" . ucfirst($name);

        try {
            $firestore = new FirestoreClient();
            $collection = $firestore->collection($class_name::COLLECTION);
            $snapshot = $collection->documents();
            foreach ($snapshot as $item) {
                $obj = $entityManager->getRepository($class_name)->findOneBy(['firebaseId' => $item->id()]);
                if ($obj) {
                    $obj->setFirebaseId(null);
                    $entityManager->persist($obj);
                    $entityManager->flush();

                }

                // remove playlist referenced to device
                if ($class_name::COLLECTION === Device::COLLECTION) {
                    $item['playlist']->delete();
                }

                if ($class_name::COLLECTION === Partner::COLLECTION) {
                    $devices = $firestore->document($class_name::COLLECTION . '/' . $item->id())->collection('devices')->documents();
                    var_dump($devices);
//                    foreach ($devices as $device) {
//                        $device->delete();
//                    }
                }

                $firestore->document($class_name::COLLECTION . '/' . $item->id())->delete();
            }
        } catch (GoogleException $e) {

        } catch (\Exception $e) {

        }

        return $this->redirectToRoute(sprintf('%s_index', $name));
    }


    /**
     * @Route("/lorem", name="lorem", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param $name
     * @return Response
     */
    public function lorem(Request $request, KernelInterface $kernel): Response
    {
        $expires = new \DateTime(  '+ 10 years');
        $storage = new StorageClient();
        $bucket = $storage->bucket($this->getParameter('bucket_name'));

        $sample = $kernel->getProjectDir() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'uploads/videos/sample.mp4';

        $object = $bucket->upload(fopen($sample, 'r'), [
            'name' => $this->getParameter('bucket_videos_directory') . DIRECTORY_SEPARATOR . 'sample.mp4',
            'metadata' => [
                'contentType' => 'video/mp4'
            ]
        ]);

        echo $object->signedUrl($expires);


        return new Response("");
    }

}
