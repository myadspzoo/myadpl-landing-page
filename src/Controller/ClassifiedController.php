<?php

namespace App\Controller;

use App\Entity\Classified;
use App\Form\ClassifiedType;
use App\Repository\ClassifiedRepository;
use App\Service\Interfaces\PlaylistCollectionProviderInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/classified")
 */
class ClassifiedController extends AbstractController
{
    /**
     * @Route("/", name="classified_index", methods={"GET"})
     * @param ClassifiedRepository $classifiedRepository
     * @return Response
     */
    public function index(ClassifiedRepository $classifiedRepository): Response
    {
        return $this->render('classified/index.html.twig', [
            'classifieds' => $classifiedRepository->findBy([], ['id' => Criteria::DESC])
        ]);
    }

    /**
     * @Route("/new", name="classified_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $classified = new Classified();
        $form = $this->createForm(ClassifiedType::class, $classified);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($classified);
            $entityManager->flush();

            return $this->redirectToRoute('classified_index');
        }

        return $this->render('classified/new.html.twig', [
            'classified' => $classified,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="classified_show", methods={"GET"})
     * @param Classified $classified
     * @return Response
     */
    public function show(Classified $classified): Response
    {
        return $this->render('classified/show.html.twig', [
            'classified' => $classified,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="classified_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Classified $classified
     * @return Response
     */
    public function edit(Request $request, Classified $classified): Response
    {
        $form = $this->createForm(ClassifiedType::class, $classified);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('classified_index', [
                'id' => $classified->getId(),
            ]);
        }

        return $this->render('classified/edit.html.twig', [
            'classified' => $classified,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="classified_delete", methods={"DELETE"})
     * @param Request $request
     * @param Classified $classified
     * @return Response
     */
    public function delete(Request $request, Classified $classified): Response
    {
        if ($this->isCsrfTokenValid('delete' . $classified->getId(), $request->request->get('_token'))) {
            if (!is_null($classified->getFirebaseId())) {
                try {
                    $firestore = new FirestoreClient();
                    $firestore->document(Classified::COLLECTION . '/' . $classified->getFirebaseId())->delete();
                } catch (GoogleException $e) {

                } catch (\Exception $e) {

                }
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($classified);
            $entityManager->flush();
        }

        return $this->redirectToRoute('classified_index');
    }

    /**
     * @Route("/{id}/sync", name="classified_sync", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param PlaylistCollectionProviderInterface $playlistCollectionProvider
     * @param Classified $classified
     * @return Response
     */
    public function sync(EntityManagerInterface $entityManager, PlaylistCollectionProviderInterface $playlistCollectionProvider, Classified $classified): Response
    {
        try {
            $firestore = new FirestoreClient();
            $item = $firestore->collection(Classified::COLLECTION)->add([
                'header' => $classified->getHeader(),
                'description' => $classified->getDescription(),
                'city' => $classified->getCity(),
                'phone' => $classified->getPhone(),
                'shoper_product_id' => null,
            ]);

            $classified->setFirebaseId($item->id());

            $entityManager->persist($classified);
            $entityManager->flush();

            $documents = $playlistCollectionProvider->find($classified);
            $data = $classified->prepareDataForFirebase($item, $documents->size());

            foreach ($documents as $document) {
                $document->reference()->collection('items')->add($data);
            }

        } catch (GoogleException $e) {
            // TODO: exception implementation
        } catch (\Exception $e) {
            // TODO: exception implementation
        }

        return $this->redirectToRoute('classified_index');
    }
}
