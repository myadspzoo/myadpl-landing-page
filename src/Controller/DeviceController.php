<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\Playlist;
use App\Form\DeviceType;
use App\Repository\DeviceRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/device")
 */
class DeviceController extends AbstractController
{
    /**
     * @Route("/", name="device_index", methods={"GET"})
     * @param DeviceRepository $deviceRepository
     * @return Response
     */
    public function index(DeviceRepository $deviceRepository): Response
    {
        return $this->render('device/index.html.twig', [
            'devices' => $deviceRepository->findBy([], ['id' => Criteria::DESC])
        ]);
    }

    /**
     * @Route("/new", name="device_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $device = new Device();
        $form = $this->createForm(DeviceType::class, $device);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($device);
            $entityManager->flush();

            return $this->redirectToRoute('device_index');
        }

        return $this->render('device/new.html.twig', [
            'device' => $device,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_show", methods={"GET"})
     * @param Device $device
     * @return Response
     */
    public function show(Device $device): Response
    {
        return $this->render('device/show.html.twig', [
            'device' => $device,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="device_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Device $device
     * @return Response
     */
    public function edit(Request $request, Device $device): Response
    {
        $form = $this->createForm(DeviceType::class, $device);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            if (!is_null($device->getFirebaseId())) {
                try {
                    $firestore = new FirestoreClient();
                    $firestore->document(Device::COLLECTION . '/' . $device->getFirebaseId())->update([
                        ['path' => 'android_id', 'value' => $device->getAndroidId()],
                        ['path' => 'region', 'value' => $device->getRegion()],
//                        ['path' => 'shoper_id', 'value' => $device->getShoperId()],
                        ['path' => 'shoper_id', 'value' => null],
                        ['path' => 'category', 'value' => $device->getCategory()],
                        ['path' => 'color', 'value' => $device->getColor()],
                        ['path' => 'enabled', 'value' => $device->getEnabled()],
                    ]);
                } catch (GoogleException $e) {

                } catch (\Exception $e) {

                }

            }
            return $this->redirectToRoute('device_index');
        }

        return $this->render('device/edit.html.twig', [
            'device' => $device,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_delete", methods={"DELETE"})
     * @param Request $request
     * @param Device $device
     * @return Response
     */
    public function delete(Request $request, Device $device): Response
    {
        if ($this->isCsrfTokenValid('delete' . $device->getId(), $request->request->get('_token'))) {
            if (!is_null($device->getFirebaseId())) {
                try {
                    $firestore = new FirestoreClient();
                    $firestore->document($device->getPlaylistReference())->delete();
                    $firestore->document(Device::COLLECTION . '/' . $device->getFirebaseId())->delete();
                } catch (GoogleException $e) {

                } catch (\Exception $e) {

                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($device);
            $entityManager->flush();
        }

        return $this->redirectToRoute('device_index');
    }

    /**
     * @Route("/{id}/sync", name="device_sync", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param Device $device
     * @return Response
     */
    public function sync(EntityManagerInterface $entityManager, Device $device): Response
    {
        try {
            $firestore = new FirestoreClient();

            $playlistData = [
                'region' => $device->getRegion(),
                'category' => $device->getCategory(),
            ];

            $item = $firestore->collection(Playlist::COLLECTION)->add($playlistData);
            $playlistReference = $firestore->document($item->path());

            $deviceData = [
                'id' => $device->getId(),
                'android_id' => $device->getAndroidId(),
                'playlist' => $playlistReference,
                'region' => $device->getRegion(),
                'category' => $device->getCategory(),
                'creation_ts' => (new \DateTime())->getTimestamp(),
                'enabled' => $device->getEnabled(),
                'shoper_id' => null,
                'shoper_provision_net' => 0
            ];

            $item = $firestore->collection(Device::COLLECTION)->add($deviceData);
            $device->setFirebaseId($item->id());
            $device->setPlaylistReference($playlistReference->path());

            $entityManager->persist($device);
            $entityManager->flush();
        } catch (GoogleException $exception) {

        } catch (\Exception $exception) {

        }

        return $this->redirectToRoute('device_index');
    }
}