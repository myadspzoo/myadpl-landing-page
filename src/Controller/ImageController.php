<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageType;
use App\Repository\ImageRepository;
use App\Service\Interfaces\PlaylistCollectionProviderInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Storage\StorageClient;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/image")
 */
class ImageController extends AbstractController
{
    /**
     * @Route("/", name="image_index", methods={"GET"})
     * @param ImageRepository $imageRepository
     * @return Response
     */
    public function index(ImageRepository $imageRepository): Response
    {
        return $this->render('image/index.html.twig', [
            'images' => $imageRepository->findBy([], ['id' => Criteria::DESC])
        ]);
    }

    /**
     * @Route("/new", name="image_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $image = new Image();
        $form = $this->createForm(ImageType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($image);
            $entityManager->flush();

            return $this->redirectToRoute('image_index');
        }

        return $this->render('image/new.html.twig', [
            'image' => $image,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="image_show", methods={"GET"})
     * @param Image $image
     * @return Response
     */
    public function show(Image $image): Response
    {
        return $this->render('image/show.html.twig', [
            'image' => $image,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="image_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function edit(Request $request, Image $image): Response
    {
        $form = $this->createForm(ImageType::class, $image);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('image_index', [
                'id' => $image->getId(),
            ]);
        }

        return $this->render('image/edit.html.twig', [
            'image' => $image,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="image_delete", methods={"DELETE"})
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function delete(Request $request, Image $image): Response
    {
        if ($this->isCsrfTokenValid('delete' . $image->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($image);
            $entityManager->flush();
        }

        return $this->redirectToRoute('image_index');
    }

    /**
     * @Route("/{id}/sync", name="image_sync", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param PlaylistCollectionProviderInterface $playlistCollectionProvider
     * @param Image $image
     * @return Response
     */
    public function sync(EntityManagerInterface $entityManager, PlaylistCollectionProviderInterface $playlistCollectionProvider, Image $image): Response
    {
        try {
//            $expires = new \DateTime(  '+ 10 years');
            $storage = new StorageClient();
            $name = $this->getParameter('bucket_images_directory') . DIRECTORY_SEPARATOR . $image->getSource();
            $uuid = Uuid::uuid4();
            $bucket = $storage->bucket($this->getParameter('bucket_name'));
            $object = $bucket->upload(fopen($image->getSourceAbsolutePath(), 'r'), [
                'name' => $name,
                'metadata' => [
                    'contentType' => $image->getContentType(),
                    'metadata' => [
                        //https://stackoverflow.com/questions/42956250/get-download-url-from-file-uploaded-with-cloud-functions-for-firebase/56010225#56010225
                        'firebaseStorageDownloadTokens' => $uuid
                    ]
                ]
            ]);

            $bucketUrl = $object->gcsUri();
            $downloadUrl = sprintf("https://firebasestorage.googleapis.com/v0/b/%s/o/%s?alt=media&token=%s", $this->getParameter('bucket_name'), urlencode($name), $uuid);
//            $downloadUrl = $object->signedUrl($expires);

            $firestore = new FirestoreClient();
            $item = $firestore->collection(Image::COLLECTION)->add([
                'url' => $bucketUrl,
                'downloadUrl' => $downloadUrl,
            ]);

            $image
                ->setFirebaseId($item->id())
                ->setDownloadUrl($downloadUrl)
                ->setUrl($bucketUrl);

            $entityManager->persist($image);
            $entityManager->flush();

            $documents = $playlistCollectionProvider->find($image);

            $data = $image->prepareDataForFirebase($item, $documents->size());

            foreach ($documents as $document) {
                $document->reference()->collection('items')->add($data);
            }

        } catch (GoogleException $e) {
            // TODO: exception implementation
        } catch (\Exception $e) {
            // TODO: exception implementation
        }

        return $this->redirectToRoute('image_index');
    }

}
