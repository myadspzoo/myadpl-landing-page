<?php

namespace App\Controller;

use App\Form\AdvertiserType;
use App\Form\InvestorType;
use App\Form\ContactPartnerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $advertiserForm = $this->createForm(AdvertiserType::class);
        $partnerForm = $this->createForm(ContactPartnerType::class);
        $investorForm = $this->createForm(InvestorType::class);
        return $this->render('page/index.html.twig', [
            'advertiserForm' => $advertiserForm->createView(),
            'partnerForm' => $partnerForm->createView(),
            'investorForm' => $investorForm->createView()
        ]);
    }

    /**
     * @Route("/api/sendEmailAsAdvertiser", name="send_email_as_advertiser", methods={"POST"})
     */
    public function sendEmailAsAdvertiser(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(AdvertiserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $message = (new \Swift_Message('Nowa wiadomość od reklamodawcy'))
                ->setFrom($this->getParameter('email_from'))
                ->setTo($this->getParameter('email_to'))
                ->setBody($this->renderView(
                    'page/advertiser.html.twig', [
                        'data' => $data,
                    ]
                ), 'text/html');

            $mailer->send($message);
        }

        return new Response('success');
    }

    /**
     * @Route("/api/sendEmailAsPartner", name="send_email_as_partner", methods={"POST"})
     */
    public function sendEmailAsPartner(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(ContactPartnerType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $message = (new \Swift_Message('Nowa wiadomość od partnera'))
                ->setFrom($this->getParameter('email_from'))
                ->setTo($this->getParameter('email_to'))
                ->setBody($this->renderView(
                    'page/partner.html.twig', [
                        'data' => $data,
                    ]
                ), 'text/html');

            $mailer->send($message);
        }

        return new Response('success');
    }

    /**
     * @Route("/api/sendEmailAsInvestor", name="send_email_as_investor", methods={"POST"})
     */
    public function sendEmailAsInvestor(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(InvestorType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $message = (new \Swift_Message('Nowa wiadomość od inwestora'))
                ->setFrom($this->getParameter('email_from'))
                ->setTo($this->getParameter('email_to'))
                ->setBody($this->renderView(
                    'page/investor.html.twig', [
                        'data' => $data,
                    ]
                ), 'text/html');

            $mailer->send($message);
        }

        return new Response('success');
    }

}
