<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\Partner;
use App\Form\PartnerType;
use App\Repository\PartnerRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/partner")
 */
class PartnerController extends AbstractController
{

    /**
     * @Route("/", name="partner_index", methods={"GET"})
     * @param PartnerRepository $partnerRepository
     * @return Response
     */
    public function index(PartnerRepository $partnerRepository): Response
    {
        return $this->render('partner/index.html.twig', [
            'partners' => $partnerRepository->findBy([], ['id' => Criteria::DESC])
        ]);
    }

    /**
     * @Route("/new", name="partner_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $partner = new Partner();
        $form = $this->createForm(PartnerType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($partner);
            $entityManager->flush();

            return $this->redirectToRoute('partner_index');
        }

        return $this->render('partner/new.html.twig', [
            'partner' => $partner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="partner_show", methods={"GET"})
     * @param Partner $partner
     * @return Response
     */
    public function show(Partner $partner): Response
    {
        return $this->render('partner/show.html.twig', [
            'partner' => $partner,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="partner_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Partner $partner
     * @param EntityManagerInterface $em
     * @return Response
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function edit(Request $request, Partner $partner, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(PartnerType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->getConnection()->beginTransaction(); // suspend auto-commit
            try {
                $em->persist($partner);
                $em->flush();

                $devices = $form->getData()->getDevices();

                foreach ($devices as $device) {
                    $device->setPartner($partner);
                    $em->persist($device);
                    $em->flush();
                }

                $em->getConnection()->commit();
            } catch (\Exception $e) {
                $em->getConnection()->rollBack();
                throw $e;
            }

            if (!is_null($partner->getFirebaseId())) {
                try {
                    $firestore = new FirestoreClient();
                    $firestore->document(Partner::COLLECTION . '/' . $partner->getFirebaseId())->update([
                        ['path' => 'name', 'value' => $partner->getName()],
                        ['path' => 'contact.name', 'value' => $partner->getContactName()],
                        ['path' => 'contact.email', 'value' => $partner->getContactEmail()],
                        ['path' => 'contact.phone', 'value' => $partner->getContactPhone()],
                        ['path' => 'address.city', 'value' => $partner->getAddressCity()],
                        ['path' => 'address.postal', 'value' => $partner->getAddressPostal()],
                        ['path' => 'address.street', 'value' => $partner->getAddressStreet()],
                        ['path' => 'business.category', 'value' => $partner->getBusinessCategory()],
                        ['path' => 'business.domain', 'value' => $partner->getBusinessDomain()],
                        ['path' => 'active', 'value' => $partner->getActive()],
                    ]);
                } catch (GoogleException $e) {

                } catch (\Exception $e) {

                }

            }
            return $this->redirectToRoute('partner_index');
        }

        return $this->render('partner/edit.html.twig', [
            'partner' => $partner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="partner_delete", methods={"DELETE"})
     * @param Request $request
     * @param Partner $partner
     * @return Response
     */
    public function delete(Request $request, Partner $partner): Response
    {
        if ($this->isCsrfTokenValid('delete' . $partner->getId(), $request->request->get('_token'))) {
            if (!is_null($partner->getFirebaseId())) {
                try {
                    $firestore = new FirestoreClient();
                    $firestore->document(Partner::COLLECTION . '/' . $partner->getFirebaseId())->delete();
                } catch (GoogleException $e) {

                } catch (\Exception $e) {

                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($partner);
            $entityManager->flush();
        }

        return $this->redirectToRoute('partner_index');
    }


    /**
     * @Route("/{id}/sync", name="partner_sync", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param Partner $partner
     * @return Response
     */
    public function sync(EntityManagerInterface $entityManager, Partner $partner): Response
    {
        $data = [
            'id' => $partner->getId(),
            'name' => $partner->getName(),
            'address' => [
                'street' => $partner->getAddressStreet(),
                'postal' => $partner->getAddressPostal(),
                'city' => $partner->getAddressCity(),
            ],
            'business' => [
                'category' => $partner->getBusinessCategory(),
                'domain' => $partner->getBusinessDomain(),
            ],
            'contact' => [
                'name' => $partner->getContactName(),
                'email' => $partner->getContactEmail(),
                'phone' => $partner->getContactPhone()
            ],
            'creation_ts' => (new \DateTime())->getTimestamp(),
            'active' => $partner->getActive()
        ];

        try {
            $firestore = new FirestoreClient();
            $item = $firestore->collection(Partner::COLLECTION)->add($data);
            $partner->setFirebaseId($item->id());

            $entityManager->persist($partner);
            $entityManager->flush();

            $devices = $partner->getDevices();
            if (count($devices) > 0) {
                foreach ($devices as $device) {
                    $document = $firestore->collection(Device::COLLECTION)->document($device->getFirebaseId());
                    $item->collection('devices')->add([
                        'id' => $document
                    ]);
                }
            }
        } catch (GoogleException $e) {

        } catch (\Exception $e) {

        }

        return $this->redirectToRoute('partner_index');
    }

    /**
     * @Route("/{partner}/unassign/{device}", name="partner_unassign", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param Partner $partner
     * @return Response
     */
    public function unassign(EntityManagerInterface $entityManager, Partner $partner, Device $device): Response
    {
        $partner->removeDevice($device);
        $entityManager->persist($partner);
        $entityManager->flush();

        return $this->redirectToRoute('partner_show', ['id' => $partner->getId()]);
    }
}
