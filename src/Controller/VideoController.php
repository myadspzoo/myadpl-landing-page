<?php

namespace App\Controller;

use App\Entity\Video;
use App\Form\VideoType;
use App\Repository\VideoRepository;
use App\Service\Interfaces\PlaylistCollectionProviderInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Storage\StorageClient;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/video")
 */
class VideoController extends AbstractController
{
    /**
     * @Route("/", name="video_index", methods={"GET"})
     * @param VideoRepository $videoRepository
     * @return Response
     */
    public function index(VideoRepository $videoRepository): Response
    {
        return $this->render('video/index.html.twig', [
            'videos' => $videoRepository->findBy([], ['id' => Criteria::DESC])
        ]);
    }

    /**
     * @Route("/new", name="video_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($video);
            $entityManager->flush();

            return $this->redirectToRoute('video_index');
        }

        return $this->render('video/new.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="video_show", methods={"GET"})
     * @param Video $video
     * @return Response
     */
    public function show(Video $video): Response
    {
        return $this->render('video/show.html.twig', [
            'video' => $video,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="video_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Video $video
     * @return Response
     */
    public function edit(Request $request, Video $video): Response
    {
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('video_index', [
                'id' => $video->getId(),
            ]);
        }

        return $this->render('video/edit.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="video_delete", methods={"DELETE"})
     * @param Request $request
     * @param Video $video
     * @return Response
     */
    public function delete(Request $request, Video $video): Response
    {
        if ($this->isCsrfTokenValid('delete'.$video->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($video);
            $entityManager->flush();
        }

        return $this->redirectToRoute('video_index');
    }

    /**
     * @Route("/{id}/sync", name="video_sync", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param PlaylistCollectionProviderInterface $playlistCollectionProvider
     * @param Video $video
     * @return Response
     */
    public function sync(EntityManagerInterface $entityManager, PlaylistCollectionProviderInterface $playlistCollectionProvider, Video $video): Response
    {
        try {
//            $expires = new \DateTime(  '+ 10 years');
            $storage = new StorageClient();
            $name = $this->getParameter('bucket_videos_directory') . DIRECTORY_SEPARATOR . $video->getSource();
            $uuid = Uuid::uuid4();
            $bucket = $storage->bucket($this->getParameter('bucket_name'));
            $object = $bucket->upload(fopen($video->getSourceAbsolutePath(), 'r'), [
                'name' => $name,
                'metadata' => [
                    'contentType' => $video->getContentType(),
                    'metadata' => [
                        //https://stackoverflow.com/questions/42956250/get-download-url-from-file-uploaded-with-cloud-functions-for-firebase/56010225#56010225
                        'firebaseStorageDownloadTokens' => $uuid
                    ]
                ]
            ]);

            $bucketUrl = $object->gcsUri();
            $downloadUrl = sprintf("https://firebasestorage.googleapis.com/v0/b/%s/o/%s?alt=media&token=%s", $this->getParameter('bucket_name'), urlencode($name), $uuid);
//            $downloadUrl = $object->signedUrl($expires);

            $firestore = new FirestoreClient();
            $item = $firestore->collection(Video::COLLECTION)->add([
                'url' => $bucketUrl,
                'downloadUrl' => $downloadUrl,
            ]);

            $video
                ->setFirebaseId($item->id())
                ->setDownloadUrl($downloadUrl)
                ->setUrl($bucketUrl);

            $entityManager->persist($video);
            $entityManager->flush();

            $documents = $playlistCollectionProvider->find($video);

            $data = $video->prepareDataForFirebase($item, $documents->size());

            foreach ($documents as $document) {
                $document->reference()->collection('items')->add($data);
            }

        } catch (GoogleException $e) {
            // TODO: exception implementation
        } catch (\Exception $e) {
            // TODO: exception implementation
        }
        
        return $this->redirectToRoute('video_index');
    }
}
