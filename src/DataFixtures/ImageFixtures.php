<?php

namespace App\DataFixtures;

use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;

class ImageFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $sample = __DIR__ . '/../../assets/fixtures/sample.png';
        $filesystem = new Filesystem();

        for($i = 0; $i< 5; $i++)
        {
            $file = sha1(uniqid(mt_rand(), true)) . '.png';
            $uploaded = __DIR__ . '/../../public/uploads/images/' . $file;

            $filesystem->copy($sample, $uploaded);
            $image = new Image();
            $image
                ->setRegion('PL/MAZO')
                ->setCategory('ALL')
                ->setStartedAt(new \DateTime('+ 3 days'))
                ->setDuration(7)
                ->setPrice(1800)
                ->setSource($file);

            $manager->persist($image);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['ads'];
    }
}
