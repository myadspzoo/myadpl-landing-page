<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('marcin.boron@myad.pl');
        $user->setPassword('$argon2i$v=19$m=1024,t=2,p=2$cnI5elRRV2k4b0dKS1MvSg$Trhjm3f2oLByILCh/mOzpSsL/VoCo9ggZ68MDauwW2c');
        $user->setRoles(['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']);

        $manager->persist($user);

        $user = new User();
        $user->setEmail('rafal.bociek@myad.pl');
        $user->setPassword('$argon2i$v=19$m=1024,t=2,p=2$NnZLaUJPZmxTWjFjZThVbQ$ro0MgEW1tnfh+uTgkmy59t7G8aH67ilrYnGEWYettek');
        $user->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);

        $user = new User();
        $user->setEmail('piotr.marzec@myad.pl');
        $user->setPassword('$argon2i$v=19$m=1024,t=2,p=2$NnZLaUJPZmxTWjFjZThVbQ$ro0MgEW1tnfh+uTgkmy59t7G8aH67ilrYnGEWYettek');
        $user->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);

        $user = new User();
        $user->setEmail('emil.paradowski@myad.pl');
        $user->setPassword('$argon2i$v=19$m=1024,t=2,p=2$NnZLaUJPZmxTWjFjZThVbQ$ro0MgEW1tnfh+uTgkmy59t7G8aH67ilrYnGEWYettek');
        $user->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);

        $manager->flush();
    }
}
