<?php

namespace App\DataFixtures;

use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;

class VideoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $sample = __DIR__ . '/../../assets/fixtures/sample.mp4';
        $filesystem = new Filesystem();

        for($i = 0; $i< 5; $i++)
        {
            $file = sha1(uniqid(mt_rand(), true)) . '.mp4';
            $uploaded = __DIR__ . '/../../public/uploads/videos/' . $file;

            $filesystem->copy($sample, $uploaded);
            $video = new Video();
            $video
                ->setRegion('PL/MAZO')
                ->setCategory('ALL')
                ->setStartedAt(new \DateTime('+ 3 days'))
                ->setDuration(14)
                ->setPrice(2500)
                ->setSource($file);

            $manager->persist($video);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['ads'];
    }
}
