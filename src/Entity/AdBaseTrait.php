<?php

namespace App\Entity;


use Google\Cloud\Firestore\DocumentReference;

trait AdBaseTrait
{
    public function prepareDataForFirebase(DocumentReference $itemReference, int $numObjects): ?array
    {
        return [
            'content_category' => 'category',
            'content_domain' => AdInterface::CONTENT_DOMAIN,
            'content_duration' => $this->getDuration(),
            'content_ts_start' => $this->calculcateTsStart(),
            'content_ts_end' => $this->calculcateTsEnd(),
            'content_type' => self::CONTENT_TYPE,
            'content_path' => $itemReference,
            'shoper_order_id' => null,
            'shoper_order_ts' => null,
            'shoper_provision_net' => $this->calculateProvision($numObjects)
        ];
    }

    private function calculateProvision($numObjects)
    {
        return (float)sprintf("%.2F", ($this->price * 0.15 / $numObjects));
    }

    private function calculcateTsStart()
    {
        return (new \DateTime($this->getStartedAt()->format('Y-m-d 00:00:00')))->getTimestamp();
    }

    private function calculcateTsEnd()
    {
        $end_date = new \DateTime($this->getStartedAt()->format('Y-m-d 00:00:00'));
        $interval_spec = sprintf("P%sD", $this->duration - 1);
        return (new \DateTime($end_date->add(new \DateInterval($interval_spec))->format('Y-m-d 23:59:59')))->getTimestamp();
    }
}