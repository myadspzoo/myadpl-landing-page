<?php

namespace App\Entity;


use Google\Cloud\Firestore\DocumentReference;

interface AdInterface
{
    const CONTENT_DOMAIN = 'myad.pl';

    public function prepareDataForFirebase(DocumentReference $itemReference, int $numObjects): ?array;
}