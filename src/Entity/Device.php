<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 */
class Device
{
    const COLLECTION = 'devices';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $androidId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shoperId;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $firebaseId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $playlistReference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partner", inversedBy="devices")
     */
    private $partner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAndroidId(): ?string
    {
        return $this->androidId;
    }

    public function setAndroidId(string $androidId): self
    {
        $this->androidId = $androidId;

        return $this;
    }

    public function getShoperId(): ?string
    {
        return $this->shoperId;
    }

    public function setShoperId(?string $shoperId): self
    {
        $this->shoperId = $shoperId;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getFirebaseId(): ?string
    {
        return $this->firebaseId;
    }

    public function setFirebaseId(?string $firebaseId): self
    {
        $this->firebaseId = $firebaseId;

        return $this;
    }

    public function getPlaylistReference(): ?string
    {
        return $this->playlistReference;
    }

    public function setPlaylistReference(?string $playlistReference): self
    {
        $this->playlistReference = $playlistReference;

        return $this;
    }

    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    public function setPartner(?Partner $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    public function __toString()
    {
        return $this->getAndroidId();
    }
}
