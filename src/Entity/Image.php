<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Google\Cloud\Firestore\DocumentReference;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Image extends MediaBase implements AdInterface
{
    const EXTENSION = 'png';
    const CONTENT_TYPE = 'image';
    const COLLECTION = 'data-images';
    const PREFIX = 'IMG';

    use AdBaseTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    protected $region;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    protected $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $downloadUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $source;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    protected $startedAt;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    protected $duration;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="float",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected $price;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $firebaseId;

    /**
     * @var UploadedFile
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/png"},
     * )
     */
    protected $sourceFile;

    /**
     * @var string
     */
    protected $tempSourceFile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getDownloadUrl(): ?string
    {
        return $this->downloadUrl;
    }

    public function setDownloadUrl(string $downloadUrl): self
    {
        $this->downloadUrl = $downloadUrl;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getFirebaseId(): ?string
    {
        return $this->firebaseId;
    }

    public function setFirebaseId(?string $firebaseId): self
    {
        $this->firebaseId = $firebaseId;

        return $this;
    }

    public function getCode(): ?string
    {
        return sprintf('%s_%s_%s_%s', self::PREFIX, $this->region, $this->duration, $this->category);
    }

    public function getContentType()
    {
        return sprintf('%s/%s', self::CONTENT_TYPE,self::EXTENSION);
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return '/uploads/images';
    }
}
