<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\HasLifecycleCallbacks()
 */
abstract class MediaBase
{
    /**
     * Set source file
     *
     * @param UploadedFile $sourceFile
     *
     */
    public function setSourceFile(UploadedFile $sourceFile = null)
    {
        $this->sourceFile = $sourceFile;
        if (isset($this->source)) {
            $this->tempSourceFile = $this->source;
            $this->source = null;
        }
    }

    /**
     * Get source file
     *
     * @return null|UploadedFile
     */
    public function getSourceFile(): ?UploadedFile
    {
        return $this->sourceFile;
    }

    /**
     * Get Absoulte Path
     * @return null|string
     */
    public function getSourceAbsolutePath()
    {
        return null === $this->source
            ? null
            : $this->getUploadRootDir() . '/' . $this->source;
    }

    /**
     * Get Header Thumbnail Web path
     * @return null|string
     */
    public function getSourceWebPath()
    {
        return null === $this->source
            ? null
            : $this->getUploadDir() . '/' . $this->source;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../public' . $this->getUploadDir();
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUploadFiles()
    {
        if (null !== $this->getSourceFile()) {
            $this->source = $this->generateUniqueFileName() . '.' . $this->getSourceFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadFiles()
    {
        if (null !== $this->getSourceFile()) {
            $this->getSourceFile()->move($this->getUploadRootDir(), $this->source);
            if (isset($this->tempSourceFile)) {
                if (file_exists($this->getUploadRootDir() . '/' . $this->tempSourceFile)) {
                    unlink($this->getUploadRootDir() . '/' . $this->tempSourceFile);
                }
                $this->tempSourceFile = null;
            }
            $this->sourceFile = null;
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->tempSourceFile = $this->getSourceAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles()
    {
        if (isset($this->tempSourceFile)) {
            unlink($this->tempSourceFile);
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     */
    public function isFileUploadedOrExists(ExecutionContextInterface $context)
    {
        if (null === $this->source && null === $this->sourceFile)
            $context->buildViolation('This value should not be blank.')
                ->atPath('sourceFile')
                ->addViolation();
    }

    /**
     * @return string
     */
    public function generateUniqueFileName()
    {
        return sha1(uniqid(mt_rand(), true));
    }

}