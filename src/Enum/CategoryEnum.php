<?php

namespace App\Enum;


abstract class CategoryEnum
{

    const HAIR = 'HAIR';
    const BAKER = 'BAKER';
    const DRUG = 'DRUG';
    const ALL = 'ALL';

    /**
     * @param bool $all
     * @return array<string>
     */
    public static function getAvailableCategories($all = false): ?array
    {
        $choices = [
            'Hairdresser, Beauty salon, SPA' => self::HAIR,
            'Bakery' => self::BAKER,
            'Pharmacy' => self::DRUG,
        ];

        if (true === $all) {
            $choices['All'] = self::ALL;
        }

        return $choices;
    }
}