<?php

namespace App\Enum;


abstract class DurationEnum
{

    const DAY = 1;
    const WEEK = 7;
    const TWO_WEEKS = 14;
    const MONTH = 30;

    /**
     * @return array<string>
     */
    public static function getAvailableDurations(): ?array
    {
        return [
            '1 day' => self::DAY,
            '7 days' => self::WEEK,
            '14 days' => self::TWO_WEEKS,
            '30 days' => self::MONTH,
        ];
    }
}