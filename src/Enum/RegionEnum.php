<?php

namespace App\Enum;


use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class RegionEnum
{
    public static function getAvailableRegions()
    {
        $cache = new FilesystemAdapter();
        $items = $cache->get('regions', function (ItemInterface $item) {
            $computed = [];
            $item->expiresAfter(60 * 60 * 24);

            try {
                $firestore = new FirestoreClient();
                $countries = $firestore->collection('country')->documents();

                foreach ($countries as $country) {
                    $voivodeships = $country->reference()->collection('voivodeship')->documents();
                    foreach ($voivodeships as $voivodeship) {
                        $counties = $voivodeship->reference()->collection('county')->documents();
                        foreach ($counties as $county) {
                            $localities = $county->reference()->collection('locality')->documents();
                            foreach ($localities as $locality) {
                                $value = sprintf("%s/%s/%s/%s", $country->id(), $voivodeship->id(), $county->id(), $locality->id());
                                $key = sprintf("%s -> %s -> %s -> %s", $country->get('name'), $voivodeship->get('name'), $county->get('name'), $locality->get('name'));
                                $computed[$key] = $value;
                            }
                        }
                    }
                }

                return $computed;

            } catch (GoogleException $exception) {

            } catch (\Exception $exception) {

            }
        });

        return $items;
    }
}