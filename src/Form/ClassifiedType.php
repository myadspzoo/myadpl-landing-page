<?php

namespace App\Form;

use App\Entity\Classified;
use App\Enum\CategoryEnum;
use App\Enum\DurationEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassifiedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('region')
            ->add('category', ChoiceType::class, [
                'required' => true,
                'placeholder' => 'Choose an option',
                'choices' => CategoryEnum::getAvailableCategories(true),
            ])
            ->add('header')
            ->add('description')
            ->add('city')
            ->add('phone')
            ->add('shoperProductId')
            ->add('startedAt')
            ->add('duration', ChoiceType::class, [
                'required' => true,
                'choices' => DurationEnum::getAvailableDurations()
            ])
            ->add('price', MoneyType::class, [
                'currency' => 'PLN'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Classified::class,
        ]);
    }
}
