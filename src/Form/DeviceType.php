<?php

namespace App\Form;

use App\Entity\Device;
use App\Enum\CategoryEnum;
use App\Enum\RegionEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('androidId')
//            ->add('shoperId')
            ->add('region', ChoiceType::class, [
                'required' => true,
                'placeholder' => 'Choose an option',
                'choices' => RegionEnum::getAvailableRegions()
            ])
            ->add('category', ChoiceType::class, [
                'required' => true,
                'placeholder' => 'Choose an option',
                'choices' => CategoryEnum::getAvailableCategories(),
            ])
            ->add('color')
            ->add('enabled')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Device::class,
        ]);
    }
}
