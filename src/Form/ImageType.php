<?php

namespace App\Form;

use App\Entity\Image;
use App\Enum\CategoryEnum;
use App\Enum\DurationEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('region')
            ->add('category', ChoiceType::class, [
                'required' => true,
                'placeholder' => 'Choose an option',
                'choices' => CategoryEnum::getAvailableCategories(true),
            ])
            ->add('sourceFile', FileType::class, [
                'label' => 'Image source (png)',
                'data_class' => null,
                'required' => false,
                'attr' => [
                    'class' => 'image_source',
                    'accept' => sprintf('%s/%s', Image::CONTENT_TYPE, Image::EXTENSION)
                ]
            ])
            ->add('startedAt', DateTimeType::class, [
                'required' => false,
                'empty_data' => null,
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd HH:mm:ss',
                'years' => range(2019, 2022),
                'attr' => [
                    'class' => 'form_datetime'
                ]
            ])
            ->add('duration', ChoiceType::class, [
                'required' => true,
                'choices' => DurationEnum::getAvailableDurations()
            ])
            ->add('price', MoneyType::class, [
                'currency' => 'PLN'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Image::class,
        ]);
    }
}
