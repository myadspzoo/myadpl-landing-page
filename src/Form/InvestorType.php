<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvestorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Imię i nazwisko',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'data-error' => 'Podaj swoje imię'
                ],

            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'data-error' => 'Podaj swój email'
                ],
            ])
            ->add('phone', TextType::class, [
                'label' => 'Numer telefonu',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'data-error' => 'Podaj swój nr telefonu'
                ],
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Twoje pytanie',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'data-error' => 'Twoja wiadomość',
                    'rows' => 4,
                    'cols' => 30
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
