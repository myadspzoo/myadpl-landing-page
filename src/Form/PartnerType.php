<?php

namespace App\Form;

use App\Entity\Device;
use App\Entity\Partner;
use App\Enum\CategoryEnum;
use App\Repository\DeviceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $partnerId = $builder->getData()->getId();

        $builder
            ->add('name')
            ->add('contactEmail')
            ->add('contactName')
            ->add('contactPhone')
            ->add('addressStreet')
            ->add('addressPostal')
            ->add('addressCity')
            ->add('businessCategory', ChoiceType::class, [
                'required' => true,
                'placeholder' => 'Choose an option',
                'choices' => CategoryEnum::getAvailableCategories(),
            ])
            ->add('businessDomain', TextType::class, [
                'required' => false,
                'empty_data' => 'services'
            ])
            ->add('active')
            ->add('devices', EntityType::class, [
                'multiple' => true,
                'class' => Device::class,
                'query_builder' => function (DeviceRepository $repository) use ($partnerId) {
                    return $repository->findUnassignedDevices($partnerId);
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Partner::class,
        ]);
    }
}
