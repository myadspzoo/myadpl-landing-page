<?php

namespace App\Repository;

use App\Entity\Device;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Device|null find($id, $lockMode = null, $lockVersion = null)
 * @method Device|null findOneBy(array $criteria, array $orderBy = null)
 * @method Device[]    findAll()
 * @method Device[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Device::class);
    }


    public function findAllSynchronized()
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.firebaseId is not null')
            ->getQuery()
            ->getResult();
    }

    public function findUnassignedDevices(int $partnerId = null)
    {
        $qb = $this->createQueryBuilder('d');

        $conditions[] = 'd.partner is null';

        if ($partnerId)
            $conditions[] = 'd.partner = ' . $qb->expr()->literal($partnerId);

        $orx = $qb->expr()->orX()->addMultiple($conditions);

        return $qb
            ->andWhere($orx)
            ->andWhere('d.firebaseId is not null');
    }
}