<?php

namespace App\Service\Interfaces;


interface PlaylistCollectionProviderInterface
{
    const COLLECTION_NAME = 'playlists';

    public function find($ad);
}