<?php

namespace App\Service;


use App\Enum\CategoryEnum;
use App\Service\Interfaces\PlaylistCollectionProviderInterface;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;

class PlaylistCollectionProvider implements PlaylistCollectionProviderInterface
{
    private $client;
    private $collection;

    public function __construct()
    {
        try {
            $this->client = new FirestoreClient();
            $this->collection = $this->client->collection(PlaylistCollectionProviderInterface::COLLECTION_NAME);
        } catch (GoogleException $e) {

        }
    }


    public function find($ad)
    {
        if ($ad->getCategory() === CategoryEnum::ALL) {
            $query = $this->collection
                ->orderBy('region')
                ->startAt([$ad->getRegion()])
                ->endAt([$ad->getRegion() . '\uf8ff']);
        } else {
            $query = $this->collection
                ->where('category', '=', $ad->getCategory())
                ->orderBy('region')
                ->startAt([$ad->getRegion()])
                ->endAt([$ad->getRegion() . '\uf8ff']);
        }

        return $query->documents();
    }
}