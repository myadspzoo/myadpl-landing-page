let Encore = require('@symfony/webpack-encore');
let CopyWebpackPlugin = require('copy-webpack-plugin');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .autoProvideVariables({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery",
    })
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .enableVersioning(Encore.isProduction())
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })
    .addEntry('admin/js/app', './assets/js/app.js')
    .addStyleEntry("admin/css/signin", ["./assets/css/signin.css"])
    .addPlugin(new CopyWebpackPlugin([
        { from: './assets/images', to: 'images' }
    ]))
;

module.exports = Encore.getWebpackConfig();
